package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepositoryc) {
        this.companyRepository = companyRepositoryc;
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company findById(Long id) {
        return companyRepository.findById(id);
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companyRepository.findByPage(page, size);
    }

    public Company addCompany(Company company) {
        return companyRepository.addCompany(company);
    }

    public Company update(Long id, Company company) {
        return companyRepository.update(id, company);
    }

    public void delete(Long id) {
        companyRepository.delete(id);
    }
}
