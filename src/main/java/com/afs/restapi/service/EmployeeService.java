package com.afs.restapi.service;

import com.afs.restapi.exception.CanNotUpdateException;
import com.afs.restapi.exception.EmployeeCreatedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    public static final String EMPLOYEE_AGE_RANGE_ERROR_MESSAGE = "Employee must be 18 to 65 years old";
    public static final String EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE = "Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created";
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        if (employee.isAgeRangeInvalid()){
            throw new EmployeeCreatedException(EMPLOYEE_AGE_RANGE_ERROR_MESSAGE);
        }
        if (employee.isAgeAndSalaryInvalid()){
            throw new EmployeeCreatedException(EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE);
        }
        return employeeRepository.insert(employee);
    }


    public void delete(long id) {
        Employee employee = employeeRepository.findById(id);
        employee.setStatus(false);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = employeeRepository.findById(id);
        if (!employeeToUpdate.getStatus()){
            throw new CanNotUpdateException();
        }
        employeeToUpdate.merge(employee);
        return employeeToUpdate;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public List<Employee> findByCompanyId(Long id) {
        return employeeRepository.findByCompanyId(id);
    }
}
