package com.afs.restapi.exception;

public class CanNotUpdateException extends RuntimeException {
    public CanNotUpdateException() {
        super("Can not update employee，employee status is false");
    }
}
