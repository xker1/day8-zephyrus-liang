package com.afs.restapi.service;

import com.afs.restapi.exception.CanNotUpdateException;
import com.afs.restapi.exception.EmployeeCreatedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);
        when(employeeRepository.insert(eq(employee)))
                .thenReturn(new Employee(1L, "Lily", age, "Female", 20000));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
        assertEquals(true, employeeResponse.getStatus());
    }

    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);

        EmployeeCreatedException employeeCreatedException =
                assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employee must be 18 to 65 years old", employeeCreatedException.getMessage());
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30, 200000", "29, 19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) {
        Employee employee = new Employee(null, "Lily", age, "Female", salary);
        when(employeeRepository.insert(eq(employee)))
                .thenReturn(new Employee(1L, "Lily", age, "Female", salary));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());
    }

    @Test
    void should_throw_exception_when_create_employee_given_age_is_30_and_salary_is_19999() {
        Employee employee = new Employee(null, "Lily", 30, "Female", 19999);

        EmployeeCreatedException employeeCreatedException =
                assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));

        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created"
                , employeeCreatedException.getMessage());
    }

    @Test
    void should_make_employee_status_false_when_delete_employee_given_employee_id() throws Exception {
        Employee employee = new Employee(1L, "Lily", 30, "Female", 19999);
        when(employeeRepository.findById(eq(1L))).thenReturn(employee);

        employeeService.delete(employee.getId());
        assertFalse(employee.getStatus());
    }

    @Test
    void should_return_can_not_modify_when_update_employee_given_active_false() {
        Employee employee = new Employee(1L, "Lily", 30, "Female", 19999);
        employee.setStatus(false);
        Employee employeeUpdate = new Employee(2L, "Lily", 19, "Female", 30000);
        when(employeeRepository.findById(1L)).thenReturn(employee);
        CanNotUpdateException canNotUpdateException = assertThrows(CanNotUpdateException.class, () -> employeeService.update(1L, employeeUpdate));
        assertEquals("Can not update employee，employee status is false", canNotUpdateException.getMessage());
    }

    @Test
    void should_get_all_employees_when_perform_get_all_given_() {
        employeeService.findAll();
        verify(employeeRepository).findAll();
    }

    @Test
    void should_return_employee_when_perform_get_by_id_given_employee_id() {
        Employee employee = new Employee(1L, "Lily", 30, "Female", 19999);
        when(employeeRepository.findById(1L)).thenReturn(employee);
        Employee employeeResponse = employeeService.findById(1L);
        assertEquals(employee, employeeResponse);
        assertEquals(employee.getId(), employeeResponse.getId());
        assertEquals(employee.getName(), employeeResponse.getName());
        assertEquals(employee.getAge(), employeeResponse.getAge());
        assertEquals(employee.getGender(), employeeResponse.getGender());
        assertEquals(employee.getSalary(), employeeResponse.getSalary());
        assertEquals(employee.getStatus(), employeeResponse.getStatus());
    }

    @Test
    void should_return_employee_list_when_perform_get_by_gender_given_gender() {
        Employee employee1 = new Employee(1L, "Lily", 30, "Female", 29999);
        ArrayList<Employee> employeeArrayList = new ArrayList<>(List.of(employee1));

        when(employeeRepository.findByGender("Female")).thenReturn(employeeArrayList);
        List<Employee> employeeListResponse = employeeService.findByGender("Female");

        assertEquals(employeeArrayList, employeeListResponse);
        assertEquals(1L, employeeListResponse.get(0).getId());
        assertEquals("Lily", employeeListResponse.get(0).getName());
        assertEquals(30, employeeListResponse.get(0).getAge());
        assertEquals("Female", employeeListResponse.get(0).getGender());
        assertEquals(29999, employeeListResponse.get(0).getSalary());
    }

    @Test
    void should_return_employee_list_when_perform_find_by_page_given_size_and_page() {
        Employee employee1 = new Employee(1L, "Lily", 30, "Female", 29999);
        Employee employee2 = new Employee(2L, "Lily", 30, "Female", 29999);
        Employee employee3 = new Employee(3L, "Lily", 30, "Female", 29999);
        Employee employee4 = new Employee(4L, "Lily", 30, "Female", 29999);
        Employee employee5 = new Employee(5L, "Lily", 30, "Female", 29999);
        ArrayList<Employee> employeeArrayList = new ArrayList<>(List.of(employee1,  employee2, employee3, employee4, employee5));

        when(employeeRepository.findByPage(1, 5)).thenReturn(employeeArrayList);
        List<Employee> employeeListResponse = employeeService.findByPage(1, 5);

        assertEquals(employeeArrayList, employeeListResponse);
        assertEquals(5, employeeListResponse.size());
        assertEquals(1L, employeeListResponse.get(0).getId());
        assertEquals("Lily", employeeListResponse.get(1).getName());
        assertEquals(30, employeeListResponse.get(2).getAge());
        assertEquals("Female", employeeListResponse.get(3).getGender());
        assertEquals(29999, employeeListResponse.get(4).getSalary());
    }
}
