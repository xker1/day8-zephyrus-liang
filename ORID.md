O:
    1. Learn how to use annotation in Integration testing, and practice how to write Integration testing through demo.
    2. Learn the basic cincepts of IOC and DI.
    3. Learn the three-layer model, including controller, service, and repository.
    4. Practice splitting the three-layer model and learn how to write tests for different levels.
R:  Today's learning content is rich and full of challenges.
I: 
    1. How to write Integration testing for the first time and how to write corresponding tests for different levels are full of challenges.
    2. Some concepts of Mock are not yet clear, and you may not know how to solve them when writing tests.
D:  The last part of today is difficult for me, and I will continue to practice and deepen my understanding after class.